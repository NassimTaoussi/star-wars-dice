import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  return runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Color(0xFFD7CCC8),
        appBar: AppBar(
          title: Text("Star Wars Dice", style: TextStyle(color: Color(0xFFFAFAFA), fontFamily: "Starjedi"), textAlign: TextAlign.center,),
          backgroundColor: Color(0xFF263238),
        ),
        body: DicePage(),
      )
    ),
  );
}

class DicePage extends StatefulWidget {
  const DicePage({Key? key}) : super(key: key);

  @override
  _DicePageState createState() => _DicePageState();
}

class _DicePageState extends State<DicePage> {
  var randomNumber = 0;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: <Widget>[
          Expanded(
            child: TextButton(
              child: Image.asset('assets/images/$randomNumber.png', width: 400, height: 400),
              onPressed: () {
                setState(() {
                  randomNumber = Random().nextInt(11);
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}

